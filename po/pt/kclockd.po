msgid ""
msgstr ""
"Project-Id-Version: kclock\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-19 00:52+0000\n"
"PO-Revision-Date: 2022-09-18 23:20+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: PowerDevil Devin Lin Han Young\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "José Nuno Pires"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "zepires@gmail.com"

#: alarm.cpp:73
#, kde-format
msgid "Dismiss"
msgstr "Esquecer"

#: alarm.cpp:73
#, kde-format
msgid "Snooze"
msgstr "Adiar"

#: alarm.cpp:75 alarm.h:51
#, kde-format
msgid "Alarm"
msgstr "Alarme"

#: alarm.cpp:77 timer.cpp:55
#, kde-format
msgid "View"
msgstr "Ver"

#: alarmmodel.cpp:222
#, kde-kuit-format
msgctxt "@info"
msgid "Alarm: <shortcut>%1 %2</shortcut>"
msgstr "Alarme: <shortcut>%1 %2</shortcut>"

#: main.cpp:30
#, kde-format
msgid "Don't use PowerDevil for alarms if it is available"
msgstr "Não usar o PowerDevil para os alarmes se estiver disponível"

#: main.cpp:45
#, kde-format
msgid "© 2020-2022 KDE Community"
msgstr "© 2020-2022 da Comunidade do KDE"

#: main.cpp:46
#, kde-format
msgid "Devin Lin"
msgstr "Devin Lin"

#: main.cpp:47
#, kde-format
msgid "Han Young"
msgstr "Han Young"

#: timer.cpp:53
#, kde-format
msgid "Timer complete"
msgstr "Temporizador completo"

#: timer.cpp:54
#, kde-format
msgid "Your timer %1 has finished!"
msgstr "O seu temporizador %1 terminou!"

#: xdgportal.cpp:34
#, kde-format
msgid ""
"Allow the clock process to be in the background and launched on startup."
msgstr ""
"Permitir ao processo do relógio estar em segundo plano e ser lançado no "
"arranque."
