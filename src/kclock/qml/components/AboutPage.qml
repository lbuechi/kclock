/*
 * Copyright 2020 Devin Lin <espidev@gmail.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

import QtQuick 2.11
import org.kde.kirigamiaddons.labs.mobileform 0.1 as MobileForm

import kclock 1.0

MobileForm.AboutPage {
    id: aboutPage
    aboutData: AboutType.aboutData
}
